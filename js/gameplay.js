/*
 * Title: gameplay.js
 * Author: Jacqueline Button
 * Description: Contains all of the code for the non-user-interface parts
 * of the game.
 */

/*
 * The Track class represents a Radar Contact
 */
class Track {
    constructor(xPos,yPos,trackName){
        this.xPos = xPos;
        this.yPos = yPos;
        this.trackName = trackName;
        this.xDist = 0.0;
        this.yDist = 0.0;
        this.distance = 0.0;
        this.displayDistance = 0.0;
        this.setDistances(determineDistances(xPos,yPos));
    }
    /* 
     * Sets the distance variables in the class
     * based on an array passed by the determineDistances
     * function.
     */
    setDistances(distances){
        this.xDist = distances[0];
        this.yDist = distances[1];
        this.distance = distances[2];
        this.displayDistance = Math.floor(distances[2] / 20);
    }
    //returns the class variables for debug purposes
    toString() {
        return this.xPos + ' | ' + this.yPos + ' | ' + this.xDist + ' | ' + this.yDist + ' | ' + this.distance + ' | ' +  this.displayDistance + ' | ';
    }
}

/*
 * The TrackSet class represents the total set of all 
 * enemy Radar Contacts. It contains a static variable
 * that is a Map() of all the Radar Contacts.
 */ 
class TrackSet {
    constructor(){
        TrackSet.tracks = new Map();
    }
    addTrack(key, value) {
        TrackSet.tracks.set(key, value);
    }
    printTracks() {
        for (var curtrack in TrackSet.tracks){
            console.log('1 + ' + curtrack);
        }
    }
}

/*
 * This class represents a player's Ship.
 */ 
class PlayerShip {
    constructor(){
        PlayerShip.health = 4;
    }
    damageShip() {
        PlayerShip.health -= 1;
    }
    checkHealth() {
        if (PlayerShip.health <= 0) {
            return false;
        } else {
            return true;
        }
    }
    resetHealth() {
        PlayerShip.health = 4;
    }
}

/*
 * Creates a specificed number of enemy Radar Contacts
 * and adds them to the TrackSet class's static variable.
 */ 
var createTracks = function(numOfTracks) {
    var tracks = new TrackSet();
    function storeTrack(trackName, newTrack) {
        tracks.addTrack(trackName, newTrack);
        console.log(TrackSet.tracks.get(trackName));
    }
    function createAndStoreRandomTrack(trackName) { 
        var xPos = Math.floor(Math.random() * 475) + 1;                
        var yPos = Math.floor(Math.random() * 475) + 1;
        while (determineDistances(xPos, yPos)[2] < 100){
            var xPos = Math.floor(Math.random() * 475) + 1;                
            var yPos = Math.floor(Math.random() * 475) + 1;           
        }

        var newTrack = new Track(xPos, yPos, trackName);
        storeTrack(trackName, newTrack);
    }
    for (var i = 0; i < numOfTracks; i++) {
        if (i < 10 ) {
            createAndStoreRandomTrack("A00" + i);
        } else if (i >= 10 && i < 100){
            createAndStoreRandomTrack("A0" + i);
        } else {
            createAndStoreRandomTrack("A" + i);
        }
        
    }
    return tracks;
}

/*
 * Calculates and returns an array of distances from a 
 * radar contact (Track class) to the player. This is 
 * used in moving the radar contact closer to the player.
 */ 
var determineDistances = function (xPos, yPos) {
    "use strict";
    var xDist = 0.0;
    var yDist = 0.0;
    var distance = 0.0;
    if (xPos < 250 && yPos < 250) {
        xDist = 250 - xPos;
        yDist = 250 - yPos;
        distance = Math.sqrt(
        Math.pow(xDist, 2.0)  +  Math.pow(yDist, 2.0));
    } else if (xPos > 250 && yPos < 250) {
        xDist = xPos - 250;
        yDist = 250 - yPos;
        distance = Math.sqrt(
            Math.pow(xDist, 2.0)  +  Math.pow(yDist, 2.0));
    } else if (xPos < 250 && yPos > 250) {
        xDist = 250 - xPos;
        yDist = yPos  - 250;
        distance = Math.sqrt(
            Math.pow(xDist, 2.0)  +  Math.pow(yDist, 2.0));
    } else if (xPos > 250 && yPos > 250) {
        xDist = xPos - 250;
        yDist = yPos - 250;
        distance = Math.sqrt(
            Math.pow(xDist, 2.0)  +  Math.pow(yDist, 2.0));
    } else if (xPos == 250 && yPos > 250) {
        xDist = 0;
        yDist = yPos - 250;
        distance = yDist;
    } else if (xPos == 250 && yPos > 250) {
        xDist = 0;
        yDist = yPos + 250;
        distance = yDist;
    } else if (xPos < 250 && yPos == 250) {
        xDist = 250 - xPos;
        yDist = 0;
        distance = xDist;
    } else if (xPos > 250 && yPos == 250) {
        xDist = xPos - 250;
        yDist = 0;
        distance = xDist;
    }
    var output = [xDist, yDist, distance];
    return output;
}

/*
 * Calculates and returns the trig angle between a Radar Contact
 * and the player.
 */
var calcAngle = function(x, y,xDist, yDist, distance){                
        var angle = 0.0;
        if (x < 250 && y < 250) {
            angle = xDist / distance;
            angle = Math.asin(angle);
        } else if (x > 250 && y < 250) {
            angle = xDist / distance;            
            angle = Math.asin(angle);            
        } else if (x < 250 && y > 250) {
            angle = xDist / distance;
            angle = Math.asin(angle);
        } else if (x > 250 && y > 250) {
            angle = xDist / distance;
            angle = Math.asin(angle);
        } else if (x == 250 && y < 250) {
            angle = Math.PI / 2;
        } else if (x == 250 && y > 250) {
            angle = 3 * (Math.PI / 2);
        } else if (x < 250 && y == 250) {
            angle = 0;
        } else if (x = 250 && y == 250) {
            angle = 2 * (Math.PI / 2);
        }
        
        return angle;
    }

/*
 * Modifies the position of a single radar contact (Track Class)
 * so that it has moved a set distance closer to the player ship.
 */
var moveTrack = function (curTrack) {
        var newXDist;
        var newYDist;
        var oldDistances = determineDistances(curTrack.xPos, curTrack.yPos);
        var x = curTrack.xPos;
        var y = curTrack.yPos;
        var oldXDist = oldDistances[0];
        var oldYDist = oldDistances[1];
        var distance = oldDistances[2];
        var angle = calcAngle(curTrack.xPos,curTrack.yPos, oldXDist,oldYDist,distance);
        distance -= 2.0;

        if (x == 250 && y < 250) {
            newYDist = distance;
            newXDist = 0;
            x = x;
            y = 250 - newYDist;
            //console.log('Q1/Q4 Boundary | newYDist: ' + newYDist + ' | x: ' + x + ' | y: ' + y);
        } 
        else if (x == 250 && y > 250) {
            newYDist = distance;
            newXDist = 0;
            x = x;
            y = 250 + newYDist;
            //console.log('Q2/Q3 Boundary | newYDist: ' + newYDist + ' | x: ' + x + ' | y: ' + y);
        }
        else if (x < 250 && y == 250){
            newXDist = distance;
            newYDist = 0;
            y = y;
            x = 250 - newXDist;
            //console.log('Q3/Q4 Boundary | newXDist: ' + newXDist + ' | x: ' + x + ' | y: ' + y);
        }
        else if (x > 250 && y == 250){
            newXDist = distance;
            newYDist = 0;
            y = y;
            x = 250 + newXDist;
            //console.log('Q1/Q2 Boundary | newXDist: ' + newXDist + ' | x: ' + x + ' | y: ' + y);
        }
        else if (x < 250 && y < 250) {
        newXDist = Math.sin(angle) * distance;
        newYDist = Math.cos(angle) * distance;
            x = 250 - newXDist;
            y = 250 - newYDist;
        }
        else if (x > 250 && y < 250) {
            newXDist = Math.sin(angle) * distance;
            newYDist = Math.cos(angle) * distance;
            x = newXDist + 250;
            y = 250 - newYDist;
        }
        else if (x < 250 && y > 250) {
            newXDist = Math.sin(angle) * distance;
            newYDist = Math.cos(angle) * distance;
            x = 250 - newXDist;
            y = newYDist + 250;
        }
        else if (x > 250 && y > 250) {

            newXDist = Math.sin(angle) * distance;
            newYDist = Math.cos(angle) * distance;

            x = newXDist + 250;
            y = newYDist + 250;
        }

        curTrack.xPos = Math.floor(x);
        curTrack.yPos = Math.floor(y);
        curTrack.xDist = newXDist;
        curTrack.yDist = newYDist;
        curTrack.setDistances(determineDistances(curTrack.xPos, curTrack.yPos));
        //console.log(curTrack.displayDistance);
        return curTrack;
}

/*
 * Determines if a ship has been hit or not.
 * if a ship has been hit and has sunk, it 
 * returns false, otherwise it returns true
 */
var checkShip = function (ship) {
    //Loop through all Radar Contacts and check for hits.
    for (var curTrack of TrackSet.tracks) {
        if (curTrack[1].displayDistance < 1) {
            console.log('Ship Hit!');
            ship.damageShip();
            TrackSet.tracks.delete(curTrack[0]);
        } 
    }
    //Check to see if the hits sink the ship.
    if (!ship.checkHealth()) {
            return false;
        } else {
            return true;
    }
    
}

/*
 * Moves all the radar contacts closer to the player.
 * It also removes Radar Contacts that have hit the ship.
 */ 
var moveAllTracks = function (trackset, ship) {
    if (TrackSet.tracks.size > 0) {

        for (var curTrack of TrackSet.tracks) {
            curTrack[1] = moveTrack(curTrack[1]);
            

            TrackSet.tracks.set(curTrack[1].trackName, curTrack[1]);      
            
        }
    }
}

var clickTrack = function(trackName) {
    console.log('track ' + trackName + ' clicked');
    TrackSet.tracks.delete(trackName);
    uiLoad(TrackSet);
     if (TrackSet.tracks.size <= 0) {
         enableWinMsg();
         enableStartButton();
     }
}

/*
 * Master control function of the game. Starts, continues
 * and ends gameplay depending on certain conditions.
 */
var playGame = function() {
    disableGameOverMsg();
    disableWinMsg();
    disableStartButton();
    var trackset = createTracks(12);
    var player = new PlayerShip();
    uiLoad(TrackSet);
    var alive = true;
    
    for (var i = 0; i < 176; i++) {
        
        setTimeout(function() {
            /*
             * Only do anything if the ship is not sunk or there are 
             * no more Radar contacts left.
             */
            if (TrackSet.tracks.size > 0) {
                //console.log("playGame() trackset is not empty");
                uiLoad(TrackSet);
                //console.log("playGame() ui refreshed x1");
                moveAllTracks(TrackSet.tracks,player);
                //console.log("playGame() tracks moved.");
                //trackset.printTracks();
                uiLoad(TrackSet);
                //console.log("playGame() ui refreshed x2");
                var shipStatus = checkShip(player);
                //console.log("playGame() ship status checked.");
                console.log(shipStatus);
                if (!shipStatus) {
                    console.log("playGame() ship status has failed.");
                    TrackSet.tracks.clear();
                    console.log("playGame() tracks erased.");
                    alive = false;
                    uiLoad(TrackSet);
                    enableStartButton();
                    player.resetHealth();
                    console.log("playGame() ui refreshed to end state.");
                    enableGameOverMsg();
                }
            }
        },2000 + i * 250);
    }
}