/*
 * Title: ui-control.js
 * Author: Jacqueline Button
 * Description: Contains all of the UI code for the game.
 */

 /*
  * Represents a range circle on the radar display.
  */
class RangeCircle {
    constructor(offset) {
        this.fromTop = 0 + offset;
        this.fromLeft = 0 + offset;
        this.diameter = 500 - offset * 2;
        this.cssClass = "radar-range-circle";
        this.id = offset + "mi-range-circle";
        this.htmlText = '<div id="' + this.id + '" class="' + this.cssClass + '" style="left: '+ this.fromLeft + 'px; top:' + this.fromTop + 'px; width:' + this.diameter + 'px; height:' + this.diameter + 'px">';
    }
}

/*
 * Represents the table containing the targeting information
 */
class TrackingTable {
    constructor(trackset) {
        this.headerRowHTML =  '<tr class="tracking-table-header-row"><td class="tracking-table-header-cell">'
                            + 'TRACK ID: </td> <td class="tracking-table-header-cell"> X: </td>'
                            + '<td class="tracking-table-header-cell"> Y: </td>'
                            + '<td class="tracking-table-header-cell">DIST:</td></tr>';
        this.trackingTableRowsHTML = '';
        for (var curTrack of trackset.tracks){
            this.trackingTableRowsHTML = this.trackingTableRowsHTML + this.generateRow(curTrack[1]);
        }
        this.innerTableHTML = this.headerRowHTML + this.trackingTableRowsHTML;
    }
    /*
     * Builds the HTML for the row containing a single track's 
     * targeting information.
     */
    generateRow(curTrack) {
        var rowHTML = '<tr class="tracking-table-row">'
                    + '<td class="tracking-table-target-name">' + curTrack.trackName +'</td>'
                    + '<td class="tracking-table-target-x">' + curTrack.xDist + '</td>'
                    + '<td class="tracking-table-target-y">' + curTrack.yDist + '</td>'
                    + '<td class="tracking-table-target-dist">' + Math.floor(curTrack.displayDistance) + 'mi</td></tr>';
        return rowHTML;
    }

}

/*
 * Creates the range circles and adds them to the Radar display.
 */ 
var genRangeCircles = function(){
    var startOffset = 0;
    var maxOffset = (500 / 2) -30;
    for (var i = startOffset; i < maxOffset; i += 30) {
        var newCircle = new RangeCircle(i);
        $('#fire-control-radar-display').append(newCircle.htmlText);
    }
}


/*
 * Generates the HTML representing a single Radar Track.
 * Takes a single instance of a Track class as a parameter.
 * Returns the HTML of the track.
 */
var genRadarTrackHTML = function (curTrack) {
    var trackHTML = '<div id="' + curTrack.trackName + '" class="target-container"'
                    + 'style="left:' + curTrack.xPos + 'px; top:' + curTrack.yPos + 'px;" onClick="clickTrack(\'' + curTrack.trackName + '\');">'
                    + '<div class="target-icon" ></div>'
                    + '<div class="target-id">' + curTrack.trackName + '</div>'
                    + '</div>';
    return trackHTML;
}

/*
 * Creates and displays the Radar Display with
 * all of the tracks and range circles.
 */
var genRadarDisplay = function(){
    var radarTracksHTML = '';
     for (var curTrack of TrackSet.tracks) {    
        radarTracksHTML = radarTracksHTML + genRadarTrackHTML(curTrack[1]);
    }    
    $('#fire-control-radar-display').html(radarTracksHTML);
    genRangeCircles();
    genShip();
}

/*
 * Creates and displays the table with all of the targeting information.
 */
var genTrackingTable = function (tracksset) {
    var newTrackingTableInnerHTML = new TrackingTable(tracksset).innerTableHTML;
    $('#tracking-table').html(newTrackingTableInnerHTML);
}

/*
 * Generates and displays the player ship.
 */ 
var genShip = function (){
    var shipHTML = '<div id="player-ship"></div>';
    $('#fire-control-radar-display').append(shipHTML);
}
/*
 * Switches the Start Button's Class and enables it to start the game.
 */ 
var enableStartButton = function() {
    $('#start-button').removeClass('start-button-disabled');
    $('#start-button').addClass('start-button-enabled');
    $('#start-button').click(function(){playGame();});
}

/*
 * Switches the Start Button's Class and prevents it from starting the game.
 */
var disableStartButton = function() {
    $('#start-button').removeClass('start-button-enabled');
    $('#start-button').addClass('start-button-disabled');
    $('#start-button').off('click');
}

/*
 * Displays the game over modal.
 */
var enableGameOverMsg = function() {
    $('#tracking-table-container').prepend('<div id="game-over-modal">GAME OVER</div>');
}

/*
 * Removes the game over modal.
 */
var disableGameOverMsg = function () {
        $('#game-over-modal').remove();
    
}

/*
 * Displays the victory modal.
 */
var enableWinMsg = function() {
    $('#tracking-table-container').prepend('<div id="win-modal">VICTORY</div>');
}

/*
 * Removes the win modal.
 */
var disableWinMsg = function () {
         $('#win-modal').remove();
}

/*
 * Master UI build function. It creates the whole gameplay UI
 * and displays it to the user. This function is run repeatedly
 * each time the UI is to be updated.
 */
var uiLoad = function(trackset){  
    genTrackingTable(trackset);
    genRadarDisplay(trackset.tracks);
    
}


