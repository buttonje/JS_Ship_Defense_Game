
/**
 * This is the main applet for the fire control game. 
 * This code (which is incomplete due to lost backups) came from a class project
 * in an "Intro to Java" course from several years ago.
 * 
 * 
 * @author (Jacqueline Button)
 */
import java.applet.*;
import java.awt.*;
import java.awt.event.*; //This imports the mouse event and key event classes.

public class MainFireControl extends Applet implements MouseListener, KeyListener, Runnable {
    public RadarScope rScope; 
    //Target testTarget = new Target(100,100,true);
    Target [] contacts;
    Thread runner;
    
    public void init() { //init is called when the applet starts (much like a constructor)
        addMouseListener(this); //registers this applet to receive mouse events
        addKeyListener(this); //registers this applet to receive key events
        rScope = new RadarScope();
        contacts = new Target[10];
      for (int k=0; k< contacts.length; k++){
         contacts[k] = new Target( (int)(Math.random()*500 + 1),(int)(Math.random()*500 + 1),true );
      }
        
        runner = new Thread(this); // putting this applet into a thread that can run in the background.
      runner.start(); //starts the thread running.
    
    }
    public void paint(Graphics g) {
        rScope.paint(g);
         for (int k=0; k< contacts.length; k++){
        contacts[k].paint(g);
        displayChart(g, k);
       
        //+ "," + contacts[k].outputData[1] + "," + contacts[k].outputData[2] + "," + contacts[k].outputData[3], 510, 30 + (10 * k));}
    } }   
   public void displayChart(Graphics g, int index ) {
        g.setColor( new Color(255,255,255));
        //draw labels
        g.drawString("T", 505, 10);
        g.drawString("X", 525, 10);
        g.drawString("Y", 565, 10);
        g.drawString("ANG", 605, 10);
        g.drawString("DIST", 645, 10);
        
        
        g.setColor( new Color(255,140,47));
        //draw lines
        g.drawLine(505, 10, 505, 400);
        g.drawLine(525, 10, 525, 400);
        g.drawLine(565, 10, 565, 400);
        g.drawLine(605, 10, 605, 400);
        g.drawLine(645, 10, 645, 400);
        
        if (contacts[index].outputData[3] < 50) {
         g.setColor(new Color(240,0,0));
         
        }
        else {
        g.setColor(new Color(0,240,0));
        }
        //draw strings
        g.drawString("" + index , 505,30 + (10 * index));
        
        g.drawString("" + contacts[index].outputData[0] , 527,30 + (10 * index));
         
        g.drawString("" + contacts[index].outputData[1], 567,30 + (10 * index));
        
        g.drawString("" + contacts[index].outputData[2], 607,30 + (10 * index));
        
        g.drawString("" + contacts[index].outputData[3], 647,30 + (10 * index)) ;  
    }
       public void run()
    {
        while(true)
        {            
            //setBackground(Color.white);
           // testTarget.move()
            for (int k=0; k< contacts.length; k++){
        contacts[k].move();
        
        contacts[k].autoHide();
        //if (contacts[k].onScreen = true) {
        //    contacts[k] = null;
        //}
    }
        repaint();
           
            
            try {Thread.sleep(500);}         //go to sleep for 30 miliseconds
            catch(InterruptedException e) { }
        }
    }

    //The next 5 methods need to be there since we implement MouseListener

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseClicked(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    //The next 3 methods need to be there since we implement KeyListener

    public void keyPressed(KeyEvent e) {
    }

    public void keyReleased(KeyEvent e) {
    }

    public void keyTyped(KeyEvent e) {
    }
}