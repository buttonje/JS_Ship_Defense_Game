
/**
 * Object Class for Radar Display
 *  This code (which is incomplete due to lost backups) came from a class project
 * in an "Intro to Java" course from several years ago.
 * @author (Jacqueline Button) 
 */

import java.awt.*;
import java.awt.event.*;
import java.applet.*;
import javax.swing.*;
public class RadarScope  {
    public void init() { //init is called when the applet starts (much like a constructor)
      
        
    }

    public void paint(Graphics g) {
         //Dimension appletSize = new Dimension();
    //appletSize.setSize(500, 500);
    //resize(appletSize);
    
        //draw background
        g.fillRect(0,0,700,500);
        
        
        paintCircles(g);
       //drawGrid(g);  
        
    }
       
       public void paintCircles(Graphics g) {
        int difference = 50;
        int newDiameter = 500;
        int x = 0;
        int y = 0;
        
        
        while (newDiameter > 50) {

        //set color to green
        g.setColor(new Color(48,190,104)); //computer green
        //g.setColor(new Color(127,255,0));  //chartreuse
        //g.setColor(new Color(173,255,47));
        
        //draw outer edge of oval
        g.fillOval(x,y,newDiameter,newDiameter);
        
        //draw inner edge of oval and refill background
        
        g.setColor(new Color(0,0,0));
        g.fillOval(x + 5, y + 5,newDiameter - 10, newDiameter- 10);
        //drawGrid(g, x + 5, y + 5, newDiameter - 10);
        //g.fillOval(100,100,300,300);
        newDiameter = newDiameter  - ( 2 * difference);
        x = x + difference;
        y = y + difference;
       
    }
}
   
    public void drawGrid(Graphics g) {
       //this.x =  x;
       //this.y = y;
       int x;
       int y;
    }
}