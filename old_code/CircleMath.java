
/**
 * A class written to verify the math calculations for the incoming missiles in the game.
 * This code (which is incomplete due to lost backups) came from a class project
 * in an "Intro to Java" course from several years ago.
 * 
 * @author (Jacquleine Button) 
 */
public class CircleMath
{
   
   /** The dimensions of the circle. */
   static int [] circleDim = new int[2];  
   
   static int [] center = new int[2];  
   public static void main (String [] args) {
       System.out.println("\f");
       int [] object = new int[4];
       circleDim[0] = 100;      //x dimensions
       circleDim[1] = 100;      // y dimensions
       
       center[0] = 50;
       center[1] = 50;
       
       //object x location
       object[0] = 60;
       //object y location
       object[1] = 75;
       double distance = 0;
       double xDist =(double) (object[0]- circleDim[0] / 2.0) ;
       double yDist =(double)(object[1] -circleDim[1] / 2.0) ;
       System.out.println(xDist);
       System.out.println(yDist);
       distance = Math.sqrt(
       Math.pow(xDist, 2.0)  +  Math.pow(yDist, 2.0));
       System.out.println("" + distance);
       //calculate the starting angle to object from center
       double angle = (double)(yDist / distance);
       object[3] = (int)(Math.toDegrees(Math.asin(angle)));
       
       int i;
       for (i=0; i < object.length; i++)
       {
           
           System.out.println("" + i + ": " +object[i]);
        }
    System.out.println("angle" + ": " +Math.toDegrees(Math.asin(angle)));
   
    //now loop to see if formula works for decreasing distance
    double newDistance = distance;
    double newXDist = xDist;
    double newYDist = yDist;
    double printDistance;
    for (i= 0; newDistance > 0; i++){
    newDistance -= 1.0;
    newXDist = (Math.cos(angle) * newDistance);
    newYDist = (Math.sin(angle) * newDistance);
   
    printDistance = Math.sqrt(
       Math.pow(newXDist, 2.0)  +  Math.pow(newYDist, 2.0));
    //System.out.println("correct Dist : " + newDistance + " ca dist: " + printDistance);
   // System.out.println("xDist : " + newXDist + " y dist: " + newYDist + " || new x: " + (50 - newXDist) + " new y: " + (50 - newYDist));
    
     } 
}

}

       