----------------------------------------------------
SHIP DEFENSE GAME
---------------------------------------------------

AUTHOR: Jacqueline Button 

GITLAB USERNAME: jebutton

VERSION 0.93

SUMMARY:
This is a project to convert an old school project (a Java Applet game) into a pure HTML/CSS/JS/JQuery game. This game is inspired the old arcade game Missile Command except instead of shooting down incoming ICBMs you're on a ship defending against incoming missiles.

The recovered backup of the Java Files are included in this package. While I did end up compeleting the applet to a fully working state as part of my semester final project, I was unable to find backups of the completed code. As a result, I had to work from a partial backup that was missing several key pieces of logic, including the final math for the incoming Radar Tracks, and the code that handled the player returning fire. But enough of it was found to make a start.

INSTRUCTIONS:

1) Open ShipDefenseGame.html in Google Chrome

1) Click the start button to begin a game.

2) Try to click all of the incoming Radar Track icons before your ship gets hit.


FUTURE ENHANCEMENTS:

1) I want to move away from just clicking on the icons and instead returning fire with missiles that do AOE damage.

2) Eventually I would like to have an options modal that allows you to set parameters about the game.